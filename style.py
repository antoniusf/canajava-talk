from pygments.style import Style
from pygments.token import Comment, Keyword, Operator, Name, String, Number, Generic
from pygments.lexers import FortranFixedLexer
from pygments.lexers.jvm import JavaLexer
from pygments.lexers.rust import RustLexer
from pygments.formatters import SvgFormatter
from pygments import highlight

class PresentationStyle (Style):

    background_color = "#f7faed"

    styles = {
        Comment: "#878472",
        Keyword: "bold #de541e",
        Keyword.Pseudo: "nobold",
        Keyword.Type: "nobold #d5b942",

        Operator: "#22223b",
        Generic: "#22223b",

        Name: "#22223b",
        Name.Label: "bold #d5b942",

        String: "#0fa3b1",
        Number: "#0fa3b1"
        #Number: "#0fb128"
    }

with open("/home/antonius/Programmieren/canajava/pnRatioAntonius/src/main.rs") as codefile:
    code = codefile.read()

f = open("pn_ratio.svg", "w")
f.write(highlight(code, RustLexer(), SvgFormatter(style=PresentationStyle, fontfamily="Source Code Pro", fontsize="24px", ystep=32)))
f.close()
